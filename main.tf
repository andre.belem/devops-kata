provider "aws" {
  region  = var.region
  profile = var.profile
}

provider "archive" {}

provider "template" {}
data "archive_file" "lambda_zip" {
  type        = "zip"
  output_path = "function/lambda.zip"

  source {
    content  = file("function/dynamo.js")
    filename = "dynamo.js"
  }

  source {
    content  = file("function/handler.js")
    filename = "handler.js"
  }
}

data "aws_caller_identity" "current" {}

data "template_file" "dynamodb_access" {
  template = file("policies/dynamodb_access.json.tpl")
  vars = {
    "region"  = var.region
    "account" = data.aws_caller_identity.current.account_id
    "table"   = var.ddb_assets_name
  }
}
resource "aws_iam_role" "lambda_role" {
  name               = "lambda_iam"
  assume_role_policy = file("policies/assume_role.json")
}

resource "aws_iam_policy" "lambda_getcountsofsold_dynamodb" {
  name   = "lambda-getcountsofsold-dynamodb"
  policy = data.template_file.dynamodb_access.rendered
}

resource "aws_iam_role_policy_attachment" "lambda_getcountofsold_dynamodb_access" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_getcountsofsold_dynamodb.arn
}

resource "aws_lambda_function" "get_count_of_sold" {
  function_name = var.getcountsofsold_name
  handler       = var.getcountsofsold_handler
  runtime       = var.getcountsofsold_runtime
  role          = aws_iam_role.lambda_role.arn

  filename         = data.archive_file.lambda_zip.output_path
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256

  environment {
    variables = var.getcountsofsold_env_variables
  }
}

resource "aws_lambda_function" "update_vehicle_sales" {
  function_name = var.updatevehiclesales_name
  handler       = var.updatevehiclesales_handler
  runtime       = var.updatevehiclesales_runtime
  role          = aws_iam_role.lambda_role.arn

  filename         = data.archive_file.lambda_zip.output_path
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256

  environment {
    variables = var.updatevehiclesales_env_variables
  }
}

resource "aws_iam_policy" "lambda_logging" {
  name   = "lambda_logging"
  path   = "/"
  policy = file("policies/lambda-logging.json")
}

resource "aws_iam_role_policy_attachment" "lambda_logging_attachment" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

resource "aws_dynamodb_table" "assets" {
  billing_mode = var.ddb_assets_billing_mode
  hash_key     = var.ddb_assets_hash
  name         = var.ddb_assets_name

  attribute {
    name = var.ddb_assets_attribute_name
    type = var.ddb_assets_attribute_type
  }

  read_capacity  = var.ddb_assets_read_capacity
  stream_enabled = var.ddb_assets_stream_enabled
  write_capacity = var.ddb_assets_write_capacity
}

resource "aws_dynamodb_table_item" "assets" {
  item       = file("policies/ddb_assets_item.json")
  table_name = aws_dynamodb_table.assets.name
  hash_key   = aws_dynamodb_table.assets.hash_key
}
