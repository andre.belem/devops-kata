{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1616325598762",
      "Action": [
        "dynamodb:UpdateItem",
        "dynamodb:GetItem",
        "dynamodb:PutItem",
        "dynamodb:Query"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:dynamodb:${region}:${account}:table/${table}"
    }
  ]
}
