region = "eu-west-2"

profile = "dev"

getcountsofsold_name = "getCountOfSold"

getcountsofsold_handler = "handler.getCountOfSold"

getcountsofsold_runtime = "nodejs12.x"

getcountsofsold_env_variables = {
  DYNAMODB_TABLE = "assets"
}

updatevehiclesales_name = "updateVehicleSales"

updatevehiclesales_handler = "handler.updateVehicleSales"

updatevehiclesales_runtime = "nodejs12.x"

updatevehiclesales_env_variables = {
  DYNAMODB_TABLE = "assets"
}

ddb_assets_name = "assets"

ddb_assets_hash = "COUNTER"

ddb_assets_billing_mode = "PROVISIONED"

ddb_assets_read_capacity = 1

ddb_assets_write_capacity = 1

ddb_assets_stream_enabled = false

ddb_assets_attribute_name = "COUNTER"

ddb_assets_attribute_type = "S"
