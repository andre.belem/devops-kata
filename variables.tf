variable "region" {
  type = string
}

variable "profile" {

}

variable "getcountsofsold_name" {
  type = string
}

variable "getcountsofsold_handler" {
  type = string
}

variable "getcountsofsold_runtime" {
  type = string
}

variable "getcountsofsold_env_variables" {
  type = map(string)
}

variable "updatevehiclesales_name" {
  type = string
}

variable "updatevehiclesales_handler" {
  type = string
}

variable "updatevehiclesales_runtime" {
  type = string
}

variable "updatevehiclesales_env_variables" {
  type = map(string)
}

variable "ddb_assets_name" {
  type = string
}

variable "ddb_assets_hash" {
  type = string
}

variable "ddb_assets_billing_mode" {
  type = string
}

variable "ddb_assets_read_capacity" {
  type = number
}

variable "ddb_assets_write_capacity" {
  type = string
}

variable "ddb_assets_stream_enabled" {
  type = bool
}

variable "ddb_assets_attribute_name" {
  type = string
}

variable "ddb_assets_attribute_type" {
  type = string
}
